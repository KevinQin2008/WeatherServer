﻿$(document).ready(function () {
    $("#btnShowDb").click(function () {
        $.ajax({
            type: "GET",
            cache: false,
            data: {
                fn: 1,      
                dtNow: Date().toString()
            },
            url: "/MontingServer.ashx",
            success: function (data) {
                $("#listInfo").html(data);
            }
        });
    });

    $("#btnClearCache").click(function () {
        $.ajax({
            type: "GET",
            cache: false,
            data: {
                fn: 3,
                dtNow: Date().toString()
            },
            url: "/MontingServer.ashx",
            success: function (data) {
                alert(data);
            }
        });
    });

    $("#btnShowCache").click(function () {
        $.ajax({
            type: "GET",
            cache: false,
            data: {
                fn: 2,      
                dtNow: Date().toString()
            },
            url: "/MontingServer.ashx",
            success: function (data) {
                $("#listInfo").html(data);
            }
        });
    });
});

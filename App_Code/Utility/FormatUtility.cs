﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

/// <summary>
/// 格式化及解析工具
/// </summary>
public class FormatUtility
{
    public static short PROVINCE = 0;
    public static short CITY = 1;
    public static short COUNTY = 2;

    public static short SIMPLE=0;
    public static short DETAILS=1;
    public static short SIMPLE_2 = 2;

    static String URL_WEATHER_SIMPLE = "http://www.weather.com.cn/data/sk/#code#.html";
    static String URL_WEATHER_DETAIL = "http://m.weather.com.cn/atad/#code#.html";
    static String URL_WEATHER_OTHER = "http://www.weather.com.cn/data/cityinfo/#code#.html";

    public FormatUtility()
    {
    }

    public static String GetWeatherTable(DataTable dt)
    {
        if (dt == null || dt.Rows.Count == 0) { return "暂无数据"; }
        StringBuilder sb = new StringBuilder("<table width=1 border=1><tr><th>代码</th><th>天气</th><th>更新时间</th><th>天气详细</th><th>更新时间</th><th>最后更新</th></tr>");

        foreach (DataRow r in dt.Rows)
        {
            sb.Append("<tr><td>"+ r["areaid"] +"</td><td>"+ r["simple"] +"</td><td>"+ r["simplePublishTime"] +"</td><td>"+ r["details"] +"</td><td>"+ r["detailsPublishTime"] +"</td><td>"+ r["lastUpdateOn"] +"</td></tr>");
        }
        sb.Append("</table>");
        dt.Clear();
        dt = null;
        return sb.ToString();
    }



    public static String GetWeatherForCache(HttpContext context)
    {

        if (context.Cache.Count==0) { return "暂无数据"; }
        StringBuilder sb = new StringBuilder("<table  width=1 border=1><tr><th>代码</th><th>类型</th><th>天气</th></tr>");        
        IDictionaryEnumerator eme= context.Cache.GetEnumerator();
        while(eme.MoveNext())
        {
            if (eme.Key.ToString().StartsWith("c_"))
            {
                sb.Append("<tr><td>" + eme.Key.ToString().Substring(2,9) + "</td><td>" + eme.Key.ToString().Substring(eme.Key.ToString().Length-1,1) + "</td><td>" + eme.Value + "</td></tr>");
            }
        }
        sb.Append("</table>");        
        return sb.ToString();
    }

    public static String GetWeatherUrl(string code, int type)
    {
        if (type == SIMPLE)
        {
            return URL_WEATHER_SIMPLE.Replace("#code#", code);
        }
        else if (type == DETAILS)
        {
            return URL_WEATHER_DETAIL.Replace("#code#", code);
        }
        else if (type == SIMPLE_2)
        {
            return URL_WEATHER_OTHER.Replace("#code#", code);
        }
        return "";
    }

    /// <summary>
    /// 获取各种列表的客户端需要的字符串表达形式
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    public static String FormatList(Object listobj, int type)
    {
        if (listobj == null ) { return ""; }

        StringBuilder sb = new StringBuilder();
        if (type >= 0)
        {
            if (type == PROVINCE)
            {
                List<Province> list = (List<Province>)listobj;
                foreach (Province p in list)
                {
                    sb.Append(p.Code + "|" + p.Name + ",");
                }
            }
            else if (type == CITY)
            {
                List<City> list = (List<City>)listobj;
                foreach (City c in list)
                {
                    sb.Append(c.Code + "|" + c.Name + ",");
                }
            }
            else if (type == COUNTY)
            {
                List<County> list = (List<County>)listobj;
                foreach (County c in list)
                {
                    sb.Append(c.Code + "|" + c.Name + ",");
                }
            }
        }
        if (sb.Length > 1) { sb = sb.Remove(sb.Length - 1, 1); }
        return sb.ToString();
    }

    /// <summary>
    /// 格式化天气时间
    /// </summary>
    /// <param name="timestr"></param>
    /// <returns></returns>
    public static DateTime FormatDateFromWeather(string timestr)
    {
        DateTime dt = DateTime.Now;

        int hour= int.Parse(timestr.Split(':')[0]);
        int minute=int.Parse(timestr.Split(':')[1]);
        
        DateTime rDate = new DateTime(dt.Year, dt.Month, dt.Day, hour, minute, 0);
        if (rDate > DateTime.Now)
        {
            rDate = rDate.AddDays(-1);
        }
        return rDate;
    }

    public static string UnitSimpleWeather(string result1,string result2)
    {
        try
        {
            LitJson.JsonData data1 = LitJson.JsonMapper.ToObject(result1)["weatherinfo"];
            LitJson.JsonData data2 = LitJson.JsonMapper.ToObject(result2)["weatherinfo"];
            LitJson.JsonData rdata = new LitJson.JsonData();
            rdata["city"] = data1["city"];
            rdata["cityid"] = data1["cityid"];
            rdata["temp"] = data1["temp"];
            rdata["WD"] = data1["WD"];
            rdata["WS"] = data1["WS"];
            rdata["SD"] = data1["SD"];
            rdata["WSE"] = data1["WSE"];
            rdata["time"] = data1["time"];

            rdata["temp1"] = data2["temp1"];
            rdata["temp2"] = data2["temp2"];
            rdata["weather"] = data2["weather"];
            rdata["img1"] = data2["img1"];
            rdata["img2"] = data2["img2"];
            rdata["ptime"] = data2["ptime"];

            LitJson.JsonData data = new LitJson.JsonData();
            data["weatherinfo"] = rdata;
            return LitJson.JsonMapper.ToJson(data);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// 区县
/// </summary>
public class County
{
	public County()
	{		
	}

    public int Id { get; set; }
    public String Name { get; set; }
    public String EnName { get; set; }
    public String Code { get;set; }
    public String CityCode { get; set; }
}
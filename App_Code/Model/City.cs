﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// 城市
/// </summary>
public class City
{
	public City()
	{
		
	}

    public int Id { get; set; }
    public String Name { get; set; }
    public String EnName { get; set; }
    public String Code { get; set; }
    public String ProvinceCode { get; set; }
}
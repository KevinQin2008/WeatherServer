﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// 省份
/// </summary>
public class Province
{
	public Province()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
	}
    

    /// <summary>
    /// ID
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 省份代码
    /// </summary>
    public String Code { get; set; }

    /// <summary>
    /// 省份名称
    /// </summary>
    public String Name { get; set; }

    /// <summary>
    /// 省份英文名称
    /// </summary>
    public String EnName { get; set; }
}
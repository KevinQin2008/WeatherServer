﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// WebRequestState 的摘要说明
/// </summary>
internal class WebRequestState
{
        public byte[] Buffer;
        public MemoryStream ms;
        public const int BufferSize = 1024;
        public Stream OrginalStream;
        public HttpWebResponse WebResponse;

        public WebRequestState()
        {
            Buffer = new byte[1024];
            ms = new MemoryStream();
        }
}
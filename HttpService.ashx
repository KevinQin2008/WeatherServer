﻿<%@ WebHandler Language="C#" Class="HttpService" %>

using System;
using System.Web;

public class HttpService : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        String Result = "{ErrorCode:-1,Message:'Unknow Function.'}";
        int fn = -1;
        if (!String.IsNullOrEmpty(context.Request["fn"]))
        {
            int.TryParse(context.Request["fn"].ToString(), out fn);
            switch (fn)
            {
                case 1:
                    Result = CacheUtility.GetInstance(context).GetProvice();
                    break;
                case 2:
                    String pCode = context.Request["code"].ToString();
                    Result = CacheUtility.GetInstance(context).GetCity(pCode);
                    break;
                case 3:
                    String cCode = context.Request["code"].ToString();
                    Result = CacheUtility.GetInstance(context).GetCounty(cCode);
                    break;
                case 4:
                    //返回所有的热门城市
                    Result = CacheUtility.GetInstance(context).GetHotCity();
                    break;
                case 5:
                    Result = DataBaseUtility.LoadAllCounty();
                    break;
                case 11:
                    String code = context.Request["code"].ToString();
                    Result = CacheUtility.GetInstance(context).GetWeather(code, FormatUtility.SIMPLE);
                    break;
                case 12:
                    String ccode = context.Request["code"].ToString();
                    Result = CacheUtility.GetInstance(context).GetWeather(ccode, FormatUtility.DETAILS);
                    break;
                default:
                    break;
            }
        }
        context.Response.Write(Result);
    }
    
    
    
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
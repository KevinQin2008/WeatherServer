﻿<%@ WebHandler Language="C#" Class="MontingServer" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Threading;

public class MontingServer : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
         context.Response.ContentType = "text/plain";
        String Result = "{ErrorCode:-1,Message:'Unknow Function.'}";
        int fn = -1;
        if (!String.IsNullOrEmpty(context.Request["fn"]))
        {
            int.TryParse(context.Request["fn"].ToString(), out fn);
            switch (fn)
            {
                case 1:
                    Result=  FormatUtility.GetWeatherTable(DataBaseUtility.ReadTop20NewWeather());
                    break;
                case 2:
                    Result = FormatUtility.GetWeatherForCache(context);
                    break;
                case 3:
                    if (CacheUtility.GetInstance(context).ClearCache())
                    {
                        Result = "Clear Cache Over";
                    }
                    else
                    {
                        Result = "Clear Cache Error"; 
                    }
                    break;
                case 4:
                    List<County> countyList=DataBaseUtility.getUnkownCounty();
                    foreach(County c in countyList)
                    {
                    	 context.Response.Write(CacheUtility.GetInstance(context).CheckCity(c.Code));
                    	 Thread.Sleep(1000);
                    }
                    break;
            }
        }                        
        context.Response.Write(Result);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}